# Github *facebook/react* Issues Viewer
A Project based on a personal task from Kumparan Academy's mentors.
Task given after Git, Docker and GraphQL topics.

## TEMP::Project Workflow Plan
### Tools
**Backend** Using `express`, `express-graphql` and `graphql` packages.
**Frontend** Using `create-react-app`, `flow` and `apollo` packages.

### WorkFlow
**Fetching**
Frontend making request with GraphQL query to backend to get data from `facebook/react` repository.
Backend Fetching data from github's api.
Backend sending response to Frontend.

**Display**
(optional) "Login Scene" to get user's personal access token, as github requires it to fetch any data with its API.
The mandatory `facebook/react` issues viewer.
(optional) "Repo Chooser Scene", if I have enough time, creating a `<TextInput />` to search for any repository besides the mandatory `facebook/react`.

**Deployment**
Using **Docker** and its cool feature `docker compose`, `docker exec` and expose Frontend's port so user / mentor can test this project's status just by `docker start`ing the Docker Registry from this repository.

## MISC::Other Information(s)
### Contributing
As this is a personal task, contribution is not expected.
If you have any suggestion or critique of my coding style or tools I use, please open issue or comment on the Merge Request I made.

Thanks!
