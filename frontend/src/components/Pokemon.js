// @flow

import React from 'react';
import {Query} from 'react-apollo';
import {gql} from 'apollo-boost';

const Pokemon = () => {
  return (
    <Query
      query={gql`
        {
          pokemon(name: "charizard") {
            name
          }
          # artworkAttributionClasses {
          #   name
          # }
        }
      `}
    >
      {({loading, error, data}) => {
        if (loading) {
          return <p>Loading</p>;
        }
        if (error) {
          return <p>{'Error :('}</p>;
        }
        // esllint-disable-next-line no-console
        console.log('>>>', data);
        return (
          <div>
            {/* {data.artworkAttributionClasses.map((item) => (
              <p>{item.name}</p>
            ))} */}
            <p>{data.pokemon.name}</p>
          </div>
        );
      }}
    </Query>
  );
};

export {Pokemon};
