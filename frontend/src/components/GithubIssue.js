// @flow

import React from 'react';

import {isoToSlashed} from '../helpers/dateRelated';

type Props = {
  issue: {
    number: number,
    cursor: string,
    createdAt: string,
    title: string,
    url: string,
    state: string,
    comments: number,
    author: {login: string, url: string},
  },
};

const GithubIssue = (props: Props) => {
  const {issue} = props;
  return (
    <div
      style={{
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        height: 100,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: 'grey',
        borderRadius: 10,
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          marginLeft: 20,
          minWidth: 64,
        }}
      >
        <p className="no-margin issue-stat">STATUS</p>
        <p
          className={`no-margin issue-stat-${
            issue.state === 'OPEN' ? 'open' : 'closed'
          }`}
        >
          {issue.state}
        </p>
      </div>
      <div
        style={{
          marginLeft: 15,
          marginBottom: 5,
          marginTop: 5,
          flex: 1,
          flexDirection: 'row',
        }}
      >
        <p>
          <span
            className="this-should-be-link"
            style={{fontWeight: '600', fontSize: 20}}
          >
            {issue.title}
          </span>
          <span style={{fontSize: 14}}>
            {`      ${issue.comments} ${
              issue.comments > 1 ? 'comments' : 'comment'
            }`}
          </span>
        </p>
        <p style={{fontSize: 14}}>
          {`#${issue.number} opened ${isoToSlashed(issue.createdAt)} by `}
          <span
            onClick={() =>
              window.open(issue.author.url, '_blank', 'noopener noreferrer')
            }
            className="this-should-be-link"
            style={{fontWeight: '600'}}
          >
            {issue.author.login}
          </span>
        </p>
      </div>
    </div>
  );
};

export {GithubIssue};
