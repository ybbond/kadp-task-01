// @flow

import React from 'react';

type Props = {
  onSubmitInfo: ({pat: string, owner: string, repository: string}) => void,
};

const Header = (props: Props) => {
  const [github, setGithub] = React.useState('');
  const [owner, setOwner] = React.useState('');
  const [repository, setRepository] = React.useState('');
  return (
    <header className="App-header">
      <div style={{display: 'flex', flexDirection: 'row'}}>
        <form
          onSubmit={() => props.onSubmitInfo({pat: github, owner, repository})}
        >
          <label>
            Github Personal Access Token:
            <input
              type="password"
              onChange={(e) => setGithub(e.target.value)}
              value={github}
            />
          </label>
          <label>
            Organization Name:
            <input
              type="text"
              onChange={(e) => setOwner(e.target.value)}
              value={owner}
            />
          </label>
          <label>
            Repository Name:
            <input
              type="text"
              onChange={(e) => setRepository(e.target.value)}
              value={repository}
            />
          </label>
          <input type="submit" value="Submit" />
        </form>
      </div>
      <p>Edit src/App.js and save to reload</p>
    </header>
  );
};

export {Header};
