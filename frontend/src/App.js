// @flow

import React from 'react';
import {ApolloProvider} from 'react-apollo';
import ApolloClient, {gql} from 'apollo-boost';
import './App.css';

import {IssuesScene} from './scenes/IssuesScene';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:4000/graphql',
});

// client
//   .query({
//     query: gql`
//       {
//         getIssueList(
//           name: "react"
//           owner: "facebook"
//           issueStatus: [OPEN]
//           last: 100
//         ) {
//           issueArray {
//             number
//             title
//             url
//           }
//           totalCount
//         }
//       }
//     `,
//   })
//   // eslint-disable-next-line no-console
//   .then((result) => console.log('>>', result));

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <IssuesScene />
      </div>
    </ApolloProvider>
  );
}

export default App;
