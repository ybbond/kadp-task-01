// @flow

const MONTHS = {
  '0': 'Jan',
  '1': 'Feb',
  '2': 'Mar',
  '3': 'Apr',
  '4': 'May',
  '5': 'June',
  '6': 'July',
  '7': 'Aug',
  '8': 'Sep',
  '9': 'Oct',
  '10': 'Nov',
  '11': 'Dec',
};

export function isoToSlashed(dateIsoString: string) {
  let date = new Date(dateIsoString);
  return `${MONTHS[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`;
}
