// @flow

import React from 'react';
import {Query} from 'react-apollo';
import {gql} from 'apollo-boost';
import {GithubIssue, Header, Paging} from '../components';

const query = gql`
  {
    getIssueList(
      name: "react"
      owner: "facebook"
      issueStatus: [OPEN]
      last: 10
    ) {
      issueArray {
        number
        cursor
        createdAt
        title
        url
        state
        comments
        author {
          login
          url
        }
      }
      totalCount
    }
  }
`;

const IssuesScene = () => {
  return (
    <Query query={query}>
      {({loading, error, data}) => {
        if (loading) {
          return <p>loading</p>;
        }
        if (error) {
          return <p>maap</p>;
        }
        return (
          <>
            <Header />
            <body>
              <div style={{display: 'flex', flexDirection: 'column'}}>
                {data.getIssueList.issueArray.map((issue) => (
                  <GithubIssue issue={issue} />
                ))}
                <Paging totalCount={data.getIssueList.totalCount} />
              </div>
            </body>
          </>
        );
      }}
    </Query>
  );
};

export {IssuesScene};
