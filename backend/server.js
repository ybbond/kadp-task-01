const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');
const expressGraphQL = require('express-graphql');
const {buildSchema} = require('graphql');

// GraphQL schema
const schema = buildSchema(`
  enum IssueState {
    OPEN
    CLOSED
  }
  scalar DateTime
  scalar URI
  type Author {
    login: String!
    url: URI!
  }
  type IssueListItem {
    title: String!
    createdAt: DateTime!
    number: Int!
    state: IssueState!
    url: URI!
    author: Author!
    comments: Int!
    cursor: String!
  }
  type IssueListReturn {
    totalCount: Int!
    issueArray: [IssueListItem]
  }
  type Query {
    getIssueList(
      name: String!,
      owner: String!,
      issueStatus: [IssueState!],
      last: Int, before: String,
      first: Int, after: String,
    ): IssueListReturn!
  }
`);

// Root Resolver
const rootValue = {
  getIssueList: async (args) => {
    const {
      owner,
      name,
      issueStatus,
      last = null,
      before = null,
      first = null,
      after = null,
    } = args;
    const URI = 'https://api.github.com/graphql';
    let promise = await fetch(URI, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${DANGEROUS_DONT_COMMIT}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: `query{
        repository(owner: "${owner}", name: "${name}") {
          issues(
            states: ${issueStatus},
            ${last ? `last: ${last},` : ''}
            ${before ? `before: "${before}",` : ''}
            ${first ? `first: ${first},` : ''}
            ${after ? `after: "${after}",` : ''}
          ) {
            totalCount
            edges {
              cursor
              node {
                title
                createdAt
                number
                state
                url
                comments {
                  totalCount
                }
                author {
                  login
                  url
                }
              }
            }
          }
        }
      }`,
      }),
    });
    let json = await promise.json();
    let {
      data: {
        repository: {
          issues: {edges, totalCount}, // edges containing [{cursor, node}]
        },
      },
    } = json;
    if (edges.length === 0) {
      return {totalCount, issueArray: []};
    }
    let issueMapped = edges.map((edge) => {
      let {title, createdAt, number, state, url, author, comments} = edge.node;
      return {
        cursor: edge.cursor,
        title,
        createdAt,
        number,
        state,
        url,
        comments: comments.totalCount,
        author: {login: author.login, url: author.url},
      };
    });
    let issueArray = issueMapped.reduceRight(
      (arr, last, index, coll) => (arr = arr.concat(last)),
      [],
    );
    return {totalCount, issueArray};
  },
};

// Create an Exrpress Server and GraphQL Endpoint
const app = express();
app.use(cors());
app.use(
  '/graphql',
  expressGraphQL({
    schema,
    rootValue,
    graphiql: true,
  }),
);
app.listen(4000, () => console.log('Server running on port 4000.'));
